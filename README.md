This is a sample app for testing RTSP streaming on Android. See the original project here: https://github.com/pedroSG94/RTSP-Server
This repo has modified the project to start streaming the camera automatically when the app starts (permissions might need to be
enabled first).